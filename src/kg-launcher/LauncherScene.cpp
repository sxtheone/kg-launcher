#include "LauncherScene.h"
#include <Profiler.h>
#include <MenuButton.h>
#include <SceneMsgTypes.hpp>
#include <SceneMsgSendString.hpp>
#include <EventManager.h>
#include <Windows.h>
#include <ActManager.h>
#include <ObjectSceneFileLoader.h>
#include "Language.h"


ReturnCodes LauncherScene::loadFile(const sxString& filename)
{
  //ReturnCodes result = ObjectScene::loadFile(filename);
  // copy-paste from ObjectScene::loadFile
  ObjectSceneFileLoader loader(id);
  ReturnCodes result = loader.processFile(filename);
  drawer = loader.getFilledObjectDrawer();
  objects = &drawer->objects;
  name = loader.getSceneName();
  sceneProps = loader.getSceneProps();
  setupSliding(loader.getSlidingSpeed(), loader.getSlidingDistance());

  loader.getReaderObject()->setSection("Config");
  loader.getReaderObject()->getStr("language", language);
  setupLangId();


  // after we have the language, setup paths
  setupGamePaths();

  return result;
}

sxBool LauncherScene::handleMsgFromOtherScene(SceneMsg* msg)
{
  if(SDL_TRUE == WindowScene::handleMsgFromOtherScene(msg)) return SDL_TRUE;

  if(SceneMsgTypes::SceneMsgSendString == msg->type){
    if(static_cast<SceneMsgSendString*>(msg)->id == langId){
      if("MENU1" == static_cast<SceneMsgSendString*>(msg)->value){
          processMenuClick(0);
          return SDL_TRUE;
      }
      if("MENU2" == static_cast<SceneMsgSendString*>(msg)->value){
          processMenuClick(1);
          return SDL_TRUE;
      }
      if("MENU3" == static_cast<SceneMsgSendString*>(msg)->value){
          processMenuClick(2);
          return SDL_TRUE;
      }
      if("MENU4" == static_cast<SceneMsgSendString*>(msg)->value){
          WinExec(("cmd /c start " + gamePaths[3]).c_str(), 0);//system(gamePaths[3].c_str());
          return SDL_TRUE;
      }
      if("MENU5" == static_cast<SceneMsgSendString*>(msg)->value){
          sxEvent event;
          event.type() = sxEventTypes::QUIT;
          getEventManager->pushEventToQueue(event);
          return SDL_TRUE;
      }
    }

    if("SWITCH_TO_HUN" == static_cast<SceneMsgSendString*>(msg)->value){
      LanguageSetting::activeLanguage = Language::HUN;
      if(language != Language::HUN){
        sceneProps.setEnabled(SDL_FALSE);
        sceneProps.setVisibility(SDL_FALSE);
      }else{
        sceneProps.setEnabled(SDL_TRUE);
        sceneProps.setVisibility(SDL_TRUE);        
      }        
      return SDL_TRUE;
    }
    if("SWITCH_TO_ENG" == static_cast<SceneMsgSendString*>(msg)->value){
      LanguageSetting::activeLanguage = Language::ENG;
      if(language != Language::ENG){
        sceneProps.setEnabled(SDL_FALSE);
        sceneProps.setVisibility(SDL_FALSE);
      }else{
        sceneProps.setEnabled(SDL_TRUE);
        sceneProps.setVisibility(SDL_TRUE);        
      }        
      return SDL_TRUE;
    }
  }
  return SDL_FALSE;
}

void LauncherScene::processMenuClick(const sxInt32 gamePathIndex)
{
  WinExec(("cmd /c start " + gamePaths[gamePathIndex]).c_str(), 0);
  //system(gamePaths[gamePathIndex].c_str());
  //sxEvent event;
  //event.type() = sxEventTypes::QUIT;
  //getEventManager->pushEventToQueue(event);
}

sxBool LauncherScene::processInputEvent(sxEvent& event)
{
  if(SDL_FALSE == sceneProps.enabled()) return SDL_FALSE;

  if( sxEventTypes::MOUSE_BUTTON_UP == event.type() ||
      sxEventTypes::MOUSE_BUTTON_DOWN == event.type() ||
      sxEventTypes::MOUSE_MOTION == event.type() )
  {
    mousePos.x = event.button().x;
    mousePos.y = event.button().y;
  }

  std::vector<Object*>::iterator objIt = drawer->objects.begin();
  while(objIt != drawer->objects.end()){
    if( SDL_TRUE == (*objIt)->objectProps.objectEnabled() &&
        SDL_TRUE == (*objIt)->objectProps.clickable() )
    {
      if(SDL_TRUE == objectHit(*objIt)){
        if(sxEventTypes::MOUSE_BUTTON_UP == event.type()) (*objIt)->objectHit(event);
        return SDL_TRUE;
      }
    }
    // the object's collisionEnabled field is not needed for mouse interaction
    ++objIt;
  }

  return sceneProps.catchAllInput();
}

sxBool LauncherScene::objectHit(Object* obj)
{
  GeneralCollisionDetector collDetector;
  /*
  if( inputPressed.type() == sxEventTypes::MOUSE_BUTTON_UP ||
      inputPressed.type() == sxEventTypes::MOUSE_BUTTON_DOWN ||
      inputPressed.type() == sxEventTypes::MOUSE_MOTION )
  {
  */
    CollisionData cData = obj->getCollisionData();
    return collDetector.objectCollidesWith(drawer->posOffset, &cData,
                                           mousePos);
  //}

  return SDL_FALSE;
}

void LauncherScene::stepAnimation(const sxUInt32 ticksPassed)
{
  WindowScene::stepAnimation(ticksPassed);
  if(SDL_FALSE == sceneProps.enabled()) return;

  std::vector<Object*>::iterator objIt = drawer->objects.begin();
  while(objIt != drawer->objects.end()){
    if( SDL_TRUE == (*objIt)->objectProps.objectEnabled() &&
        SDL_TRUE == (*objIt)->objectProps.clickable() )
    {
      if(SDL_TRUE == objectHit(*objIt)){
        CollisionInfo collInfo; // no need to fill - not used by ButtonObject
        (*objIt)->objectHit(collInfo);
      }
    }
    // the object's collisionEnabled field is not needed for mouse interaction
    ++objIt;
  }
}
void LauncherScene::setupLangId()
{
  if(language == Language::HUN) langId = Language::HUN_ID;
  if(language == Language::ENG) langId = Language::ENG_ID;
}

void LauncherScene::setupGamePaths()
{
  gamePaths.clear();
  if(language == Language::HUN){
      gamePaths.push_back("KoterGame/KoterGame.EXE");
      gamePaths.push_back("KoterSztrajkElojatek/Elojatek.EXE");
      gamePaths.push_back("KoterSztrajk/KoterSztrajk.EXE");
      gamePaths.push_back("notepad readme.txt");
  }else{ // ENG
      gamePaths.push_back("EngKoterGame/KoterGame.EXE");
      gamePaths.push_back("EngKoterSztrajkElojatek/Elojatek.EXE");
      gamePaths.push_back("EngKoterSztrajk/KoterSztrajk.EXE");
      gamePaths.push_back("notepad readme_eng.txt");
  }
}