#pragma once
#include "FileLoaderBase.h"
#include "Language.h"

class LanguageConfigFileLoader : public FileLoaderBase
{
public:
  LanguageConfigFileLoader() : language(Language::HUN) {}
  ~LanguageConfigFileLoader() {destroyReader();}
  ReturnCodes processFile(const sxString& filename);
  INIReader* getReaderObject(){return reader;}
  sxString getLanguage(){return language;}

private:
  sxString language;

  ReturnCodes processLanguage();
};

inline
ReturnCodes LanguageConfigFileLoader::processFile(const sxString& filename)
{
  ReturnCodes result = createReader(filename, SDL_FALSE); //don't check if file already loaded
  if(shouldExitFromLoader(result)){
    return result;
  }
  
  result = processLanguage();
  return result;
}

ReturnCodes LanguageConfigFileLoader::processLanguage()
{
  ReturnCodes result;
  if(SDL_FALSE == reader->setSection("Config")){
    return ReturnCodes::RC_SECTION_NOT_FOUND;
  }
  if(ReturnCodes::RC_SUCCESS == result){
    result = reader->getStr("language", language);
  }

  return result;
}