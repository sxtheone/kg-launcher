#pragma once
#include <WindowScene.h>
#include <BasicObject.h>
#include <AnimObject.h>


class LauncherScene : public WindowScene
{
public:
  LauncherScene() : langId(0) {}
  virtual ReturnCodes loadFile(const sxString& filename);
  virtual sxBool handleMsgFromOtherScene(SceneMsg* msg); // returns SDL_TRUE if msg was handled
  virtual sxBool processInputEvent(sxEvent& event);
  virtual void stepAnimation(const sxUInt32 ticksPassed);

private:
  sxVec2Int mousePos;
  std::vector<sxString> gamePaths;
  sxString language;
  sxInt32 langId;

  sxBool objectHit(Object* obj);
  void processMenuClick(const sxInt32 gamePathIndex);
  void setupLangId();
  void setupGamePaths();
};