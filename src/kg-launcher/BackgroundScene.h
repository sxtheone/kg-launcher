#pragma once
#include <BasicObject.h>
#include <AnimObject.h>
#include <SceneMsgSendString.hpp>
#include <ObjectSceneFileLoader.h>
#include "Language.h"

class BackgroundScene : public ObjectScene
{
public:
  virtual ReturnCodes loadFile(const sxString& filename);
  virtual sxBool handleMsgFromOtherScene(SceneMsg* msg); // returns SDL_TRUE if msg was handled

private:
  sxString language;
};

inline
ReturnCodes BackgroundScene::loadFile(const sxString& filename)
{
  //ReturnCodes result = ObjectScene::loadFile(filename);
  // copy-paste from ObjectScene::loadFile
  ObjectSceneFileLoader loader(id);
  ReturnCodes result = loader.processFile(filename);
  drawer = loader.getFilledObjectDrawer();
  objects = &drawer->objects;
  name = loader.getSceneName();
  sceneProps = loader.getSceneProps();
  setupSliding(loader.getSlidingSpeed(), loader.getSlidingDistance());

  loader.getReaderObject()->setSection("Config");
  loader.getReaderObject()->getStr("language", language);
  return result;
}

inline
sxBool BackgroundScene::handleMsgFromOtherScene(SceneMsg* msg)
{
  if(SceneMsgTypes::SceneMsgSendString == msg->type){
    if("SWITCH_TO_HUN" == static_cast<SceneMsgSendString*>(msg)->value){
        if(language != Language::HUN){
            sceneProps.setEnabled(SDL_FALSE);
            sceneProps.setVisibility(SDL_FALSE);
        }else{
            sceneProps.setEnabled(SDL_TRUE);
            sceneProps.setVisibility(SDL_TRUE);        
        }        
        return SDL_TRUE;
    }
    if("SWITCH_TO_ENG" == static_cast<SceneMsgSendString*>(msg)->value){
        if(language != Language::ENG){
            sceneProps.setEnabled(SDL_FALSE);
            sceneProps.setVisibility(SDL_FALSE);
        }else{
            sceneProps.setEnabled(SDL_TRUE);
            sceneProps.setVisibility(SDL_TRUE);        
        }        
      return SDL_TRUE;
    }
  }
  return SDL_FALSE;
}
