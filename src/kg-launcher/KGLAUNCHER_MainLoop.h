#pragma once
#include <MainLoop.h>

class KGLAUNCHER_MainLoop : public MainLoop
{
public:
  KGLAUNCHER_MainLoop(){init();}
  void init();
  sxBool run();

private:
  const sxString languageConfigFilename = "../language.config";
  const sxString languageConfigFilenameWithoutPath = "language.config";

  virtual void handleActChange(){}
  //void registerSceneMsgs(){}
  void registerSceneTypes();
  void registerObjectTypes(){}
  void runInnerLoopWithSDLGFX_framerateManager(sxUInt32& timePassed);
  void loadLanguageConfigFileInfo();
  void setSceneVisibilitiesAccordingToLanguageConfig();
  void saveLanguageSetting();
};

