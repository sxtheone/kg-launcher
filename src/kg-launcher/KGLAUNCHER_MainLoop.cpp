#include "KGLAUNCHER_MainLoop.h"
#include <ActManager.h>
#include <EventManager.h>
#include <Profiler.h>
#include <AudioManager.h>
#include <FileWriter.h>
#include "LauncherScene.h"
#include "BackgroundScene.h"
#include "LanguageConfigFileLoader.h"


void KGLAUNCHER_MainLoop::init()
{
  LanguageSetting::activeLanguage = Language::HUN;
  MainLoop::init();
  getProfiler->showOnScreen(SDL_FALSE);
  getProfiler->enableDebugText(SDL_FALSE);
  getEventManager->activateAccelerometerAsJoytickListening(SDL_TRUE);
  registerSceneTypes();
  registerObjectTypes();
  loadStartingAct();
  
  loadLanguageConfigFileInfo();
  setSceneVisibilitiesAccordingToLanguageConfig();
}

void KGLAUNCHER_MainLoop::registerSceneTypes()
{
  getActManager->registerSceneType("LauncherScene", new SceneFactory<LauncherScene>());
  getActManager->registerSceneType("BackgroundScene", new SceneFactory<BackgroundScene>());
}


sxBool KGLAUNCHER_MainLoop::run()
{
  //TODO: this function should be refactored after the input queue is implemented!!
  sxUInt32 timePassed = 1; // don't set to zero
  remainingTicks = 0;

  do
  {
#ifdef __sdlANDROID__
    androidFocusChangeChecks();
#endif
    handleWindowEvents();
    
    getEventManager->clearEventQueue();
    runInnerLoopWithSDLGFX_framerateManager(timePassed);
    //runInnerLoopMyFramerateManager(timePassed);
    handleActChange();
    // to prevent big timejumps when stopping for debugging
    if(1000 < timePassed){
      timePassed = static_cast<sxUInt32>(getMainConfig->getDesiredFrameStep());
      sxLOG(LL_WARNING, "timePassed value corrected. Stopped for debugging?");
    }
  }while(!getEventManager->isEventActive(sxEventTypes::QUIT));

  saveLanguageSetting();
  sxLOG(LL_TRACE, "Exiting..");

  return SDL_FALSE;
}

void KGLAUNCHER_MainLoop::runInnerLoopWithSDLGFX_framerateManager(sxUInt32& timePassed)
{
  getEventManager->update();
  draw();
  getActManager->processInputEvents();
  stepAnimation(timePassed);
  getAudioManager->update();

  timePassed = fpsManager->SDL_framerateDelay();
  getProfiler->cycleEnded();
}

void KGLAUNCHER_MainLoop::loadLanguageConfigFileInfo()
{
  LanguageConfigFileLoader reader;
  if(ReturnCodes::RC_SUCCESS == reader.processFile(languageConfigFilename)){
    LanguageSetting::activeLanguage = reader.getLanguage();
    sxLOG_T("Language config file read, value: %s", LanguageSetting::activeLanguage.c_str());
    return;
  }
  sxLOG_T("Language config file couldn't be found.");
}

void KGLAUNCHER_MainLoop::setSceneVisibilitiesAccordingToLanguageConfig()
{
  sxString msg = "SWITCH_TO_";

  if(LanguageSetting::activeLanguage == Language::ENG) msg += Language::ENG;
  else msg += Language::HUN;

  getActManager->pushSceneMsgSendStringToQueue(msg, SDL_TRUE);
}

void KGLAUNCHER_MainLoop::saveLanguageSetting()
{
  sxLOG_T("saving language file. Language: %s", LanguageSetting::activeLanguage.c_str());
  FileWriter writer;
  writer.createFileForWriting("", languageConfigFilenameWithoutPath);
  writer.createSection("Config");
  writer.addString("language", LanguageSetting::activeLanguage);
  writer.saveFile();
  writer.closeFile();
}