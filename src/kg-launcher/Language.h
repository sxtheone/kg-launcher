#pragma once
#include <sxTypes.h>
namespace Language
{
  // strings because of lazyness..
  const sxString HUN = "HUN";
  const sxString ENG = "ENG";
  const sxInt32 HUN_ID = 0;
  const sxInt32 ENG_ID = 1;
};

class LanguageSetting
{
public:
  static sxString activeLanguage;
};